/*js for project*/
$(document).ready(function(){

    $('.paralax').parallax();

    $("input[name=phone]").inputmask("+7(999)-999-99-99");

    $( ".spinner" ).spinner();

    $('.jcarousel').jcarousel({
        // Configuration goes here
    });

    $('.top-menu-selector').click(function(){
        $('header .nav li').each(function(){
            if($(this).hasClass('xs-logo-header')){

            }else{
                $(this).toggleClass('show')
            }
        })
        return false;
    })
    $('.xs-catalog-header').click(function(){
        $('.catalog-nav').toggleClass('show')
        return false;
    })


    $('.jcarousel-prev').jcarouselControl({
        target: '-=1'
    });

    $('.jcarousel-next').jcarouselControl({
        target: '+=1'
    });

    // if( $('footer').offset().top - $(this).scrollTop() < $(window).height() ){
        // $('.basket').css('position', 'relative');
    // }

    $('.basket-widget').click(function(){
        if($(this).hasClass('empty')){

        }else{
            $('.basket-body').slideToggle();
            $('.copyright').slideToggle();
            if($('.basket-widget a').html() == 'показать корзину'){
                $('.basket-widget a').html('скрыть корзину')
            }else{
                $('.basket-widget a').html('показать корзину')
            }
        }
    })

    $('a.basket-control').click(function(){
        var link = $(this)

        $.getJSON($(this).attr('href'), function(data){
            if(parseFloat(data.price) > 0 ){
                $('.basket-total-price').html(data.price)
                if(data.count == '0'){
                    link.parent().remove()
                }else{
                    link.parent().find('.count-item').html(data.count)
                }
            }else{
                $('.basket-widget').html('<span class="glyphicon glyphicon-shopping-cart"></span> Корзина пуста')
                $('.basket-widget').addClass('empty')
                $('.basket-body').slideToggle();
                $('.copyright').slideToggle();
            }
        })
        return false;
    })

    $('.promo-control-widget').click(function(){
        if($('.promo').hasClass('closed')){
            $('.promo').removeClass('closed')
            $('.promo-control-widget span').removeClass('glyphicon-arrow-down')
            $('.promo-control-widget span').addClass('glyphicon-arrow-up')
        }else{
            $('.promo').addClass('closed')
            $('.promo-control-widget span').removeClass('glyphicon-arrow-up')
            $('.promo-control-widget span').addClass('glyphicon-arrow-down')
        }
    })

    $('.horisontal-scrolled').perfectScrollbar({
//        suppressScrollX: true
        suppressScrollY: true
    });

    $('.basket').css('position', 'fixed');

    $('.ajax-form').submit(function(){
        var form = $(this)
        $.post($(this).attr('action'), $(this).serialize(), function(data){
            form.html(data)
        })

        return false;
    });

    if(window.location.href.split('#')[1]=='animate-basket'){
        $('.basket-body').slideToggle();
        $('.copyright').slideToggle();
        $('.basket-widget a').html('скрыть корзину')
        $('.basket-widget').animate({
           backgroundColor: "#bb5424",
           borderColor: '#bb5424'
        }, 500, function(){
            $('.basket-widget').animate({
               backgroundColor: "#ed7c25",
               borderColor: '#ed7c25'
            }, 500)
        } );
        setTimeout("$('.basket-body').slideUp(); $('.copyright').slideUp(); $('.basket-widget a').html('показать корзину')", 6000)
    }

    $(".to-top").click(function() {
		$('html, body').animate({
			scrollTop : "0px"
		});
	});

    return true;
});

$(window).scroll(function() {
    //чтобы кнопка вверх и меню статьи не касались футера
	if( $('footer').offset().top - $(this).scrollTop() < $(window).height() ){
		$('.basket').css('position', 'relative');
	}else{
		$('.basket').css('position', 'fixed');
	}

    if ($(this).scrollTop() > 100) {
		$('.to-top').fadeIn();
	} else {
		$('.to-top').fadeOut();
	};

	if( $('footer').offset().top - $(this).scrollTop() < $(window).height() ){
		$('.to-top').css('bottom', $('footer').height() + 50);
	}else{
		$('.to-top').css('bottom', '50px');
	}

});