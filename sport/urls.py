# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.contrib.sitemaps import GenericSitemap

from filebrowser.sites import site
from xendor.models import Page
from app.article.models import Article
from app.catalog.models import Category, Product

admin.autodiscover()

category_dict = {
    'queryset': Category.objects.filter(visible=True),
}

product_dict = {
    'queryset': Product.objects.filter(visible=True),
}

article_dict = {
    'queryset': Article.objects.all(),
}

pages_dict = {
    'queryset': Page.objects.filter(visible=True, app_extension='', in_menu=True),
}

sitemaps = {
    'products': GenericSitemap(product_dict, priority=0.8),
    'category': GenericSitemap(category_dict, priority=0.9),
    'article': GenericSitemap(article_dict, priority=0.9),
    'pages': GenericSitemap(pages_dict, priority=0.6),
}

urlpatterns = patterns('',

    url(r'^captcha/(?P<code>[\da-f]{32})/$', 'app.supercaptcha.draw'),

    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    (r'^tinymce/', include('tinymce.urls')),

    url(r'^admin/filebrowser/', include(site.urls)),

    (r'^grappelli/', include('grappelli.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^contacts/', include('app.contacts.urls')),

    url(r'^callback/', include('app.callback.urls')),

    url(r'^news/', include('app.news.urls')),

    url(r'^article/', include('app.article.urls')),

    url(r'^response/', include('app.response.urls')),

    url(r'^basket/', include('app.basket.urls')),

    url(r'^', include('app.catalog.urls')),

    # XDP-page
    url(r'^page/', include('xendor.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )
