# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from app.catalog.models import Product
from app.supercaptcha import CaptchaField


class Response(models.Model):
    """Отзывы"""

    person = models.CharField(u'Персона', max_length=255)
    text = models.TextField(u'Текст')
    answer = models.TextField(u'Ответ', blank=True, null=True)
    visible = models.BooleanField(u'Опубликовано', default=False)
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)
    show_in_list = models.BooleanField(u'Выводить в списке отзывов', default=False)

    products = models.ManyToManyField(Product, verbose_name=u'Ассоциированные товары', blank=True, null=True)

    def __unicode__(self):
        return self.person + '::' + unicode(self.created)

    def get_products(self):
        return self.products.all()

    class Meta:
        ordering = u'-created',
        verbose_name = u'отыв'
        verbose_name_plural = u'Отзывы'


class ResponseForm(forms.ModelForm):
    """Форма добавления отзыва"""

    captcha = CaptchaField(label='')

    class Meta:
        model = Response
        fields = 'person', 'text', 'products'
        widgets = {
            'person': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ваше ФИО'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Текст отзыва', 'rows': 5}),
            'products': forms.MultipleHiddenInput()
        }