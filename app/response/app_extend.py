# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='response',
    app_name=u'Отзывы',
    node_url=lambda: reverse('response-index'),
    children=[
        {
            'title': u'Добавление отзыва',
            'url': lambda: reverse('response-add'),
            'children': [],
            'in_menu': True,
            'parameters': {},
            'meta_title': u'Добавление отзыва',
            'meta_description': None
        },
        {
            'title': u'Добавление отзыва',
            'url': lambda: reverse('response-add-success'),
            'children': [],
            'in_menu': True,
            'parameters': {},
            'meta_title': u'Добавление отзыва',
            'meta_description': None
        }
    ],
    node_parameters={},
    safe_for_structure=False
)

