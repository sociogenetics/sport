# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.generic import TemplateView

from .views import Index, AddResponse, AddResponseForProduct


urlpatterns = patterns('',
    #Индекс
    url(r'^$', Index.as_view(), name = 'response-index'),

    #Добавление отзыва
    url(r'^create/$', AddResponse.as_view(), name = 'response-add'),

    url(r'^create-for-product/(?P<pk>\d+)/$', AddResponseForProduct.as_view(), name = 'response-add-for-product'),

    #Успешное оформлени отзыва
    url(r'^add-success/$', TemplateView.as_view(template_name=u'response/success.html'), name = 'response-add-success'),
)