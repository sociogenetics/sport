# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView

from xendor.cbv import PaginatedListMixin, PageAppExtensionMixin, VisibleObjectListMixin, WithMailer, ToStructureMixin
from xendor.settings import XendorSettings
from app.catalog.models import Product

from .models import Response, ResponseForm


class Index(PageAppExtensionMixin, PaginatedListMixin, VisibleObjectListMixin):
    """индекс"""

    template_name = 'response/index.html'
    model = Response
    paginate_by = 12
    app_extension = 'response'

    def get_queryset(self):
        return super(Index, self).get_queryset().filter(show_in_list=True)


class AddResponse(WithMailer, CreateView):
    """Добавление отзыва"""

    model = Response
    form_class = ResponseForm
    template_name = 'response/add.html'

    def get_success_url(self):
        return reverse('response-add-success')

    def form_valid(self, form):

        self.send_mail(
            'На сайте добавлен отзыв {site_name}',
            'response/mail.html',
            form.cleaned_data,
            XendorSettings().get(u'E-mail администратора'))

        form.instance.save()

        from django.contrib import messages
        messages.add_message(self.request, messages.SUCCESS, 'Спасибо! Заявка отправлена администрации сайта.')
        return super(AddResponse, self).form_valid(form)


class AddResponseForProduct(ToStructureMixin, AddResponse):
    """Добавление отзыва для товара"""

    meta_title = meta_description = breadcrumbs = u'Добавление отзыва о товаре'
    activated_node = lambda o: reverse('response-index')

    def get_initial(self):
        product = get_object_or_404(Product, pk=self.kwargs['pk'])
        initial = super(AddResponseForProduct, self).get_initial()
        initial.update({
            'products': [product.pk],
        })
        return initial