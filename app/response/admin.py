# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Response


class ResponseAdmin(admin.ModelAdmin):
    list_display = 'person', 'text', 'created', 'show_in_list', 'visible'
    filter_horizontal = 'products',
    raw_id_fields = 'products',


admin.site.register(Response, ResponseAdmin)