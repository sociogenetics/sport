# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings

from .models import BanerZone, Banner


class BanerZoneAdmin(admin.ModelAdmin):
#     actions = None
# 
#     def has_delete_permission(self, request, obj=None):
#         return False
# 
#     def has_add_permission(self, request, obj=None):
#         if settings.DEBUG:
#             return True
#         return False
#     
    list_display = '__unicode__', 'visible'
    list_editable = 'visible',
    

class BannerAdmin(admin.ModelAdmin):
    list_display = '__unicode__', 'visible'
    list_editable = 'visible',


# class HeadBannerAdmin(admin.ModelAdmin):
#     list_display = '__unicode__', 'visible'
#     list_editable = 'visible',
    

admin.site.register(BanerZone, BanerZoneAdmin)
admin.site.register(Banner, BannerAdmin)
# admin.site.register(HeadBanner, HeadBannerAdmin)

