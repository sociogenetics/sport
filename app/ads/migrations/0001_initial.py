# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BanerZone'
        db.create_table(u'ads_banerzone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('width', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('height', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('style', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'ads', ['BanerZone'])

        # Adding model 'Banner'
        db.create_table(u'ads_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'ads', ['Banner'])

        # Adding M2M table for field baner_zone on 'Banner'
        m2m_table_name = db.shorten_name(u'ads_banner_baner_zone')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('banner', models.ForeignKey(orm[u'ads.banner'], null=False)),
            ('banerzone', models.ForeignKey(orm[u'ads.banerzone'], null=False))
        ))
        db.create_unique(m2m_table_name, ['banner_id', 'banerzone_id'])


    def backwards(self, orm):
        # Deleting model 'BanerZone'
        db.delete_table(u'ads_banerzone')

        # Deleting model 'Banner'
        db.delete_table(u'ads_banner')

        # Removing M2M table for field baner_zone on 'Banner'
        db.delete_table(db.shorten_name(u'ads_banner_baner_zone'))


    models = {
        u'ads.banerzone': {
            'Meta': {'ordering': "('name',)", 'object_name': 'BanerZone'},
            'height': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'style': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'width': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ads.banner': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Banner'},
            'baner_zone': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['ads.BanerZone']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['ads']