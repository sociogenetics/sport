# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Banner.link'
        db.add_column(u'ads_banner', 'link',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


        # Changing field 'BanerZone.style'
        db.alter_column(u'ads_banerzone', 'style', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):
        # Deleting field 'Banner.link'
        db.delete_column(u'ads_banner', 'link')


        # User chose to not deal with backwards NULL issues for 'BanerZone.style'
        raise RuntimeError("Cannot reverse this migration. 'BanerZone.style' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'BanerZone.style'
        db.alter_column(u'ads_banerzone', 'style', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'ads.banerzone': {
            'Meta': {'ordering': "('name',)", 'object_name': 'BanerZone'},
            'height': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'style': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'width': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ads.banner': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Banner'},
            'baner_zone': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['ads.BanerZone']", 'symmetrical': 'False'}),
            'file': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['ads']