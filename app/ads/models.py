# -*- coding: utf-8 -*-
from django.db import models

from xendor.utils import getmd5


class BanerZone(models.Model):
    name = models.CharField(u'Имя зоны', max_length=255)
    width = models.CharField(u'Ширина', max_length=255)
    height = models.CharField(u'Высота', max_length=255)
    style = models.CharField(u'Стили', max_length=255, blank=True, null=True)
    visible = models.BooleanField(u'Отображать на сайте', default=True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        ordering = 'name',
        verbose_name = u'банерная зона'
        verbose_name_plural = u'Банерные зоны'


class Banner(models.Model):
    baner_zone = models.ManyToManyField(BanerZone, verbose_name='Зона')
    name = models.CharField(u'Имя банера', max_length=255)
    link = models.CharField(u'Ссылка', max_length=255, blank=True, null=True)

    file = models.ImageField(u'Картинка',
        upload_to=lambda instance, filename: getmd5(u'ads', filename),
        blank=True, null=True)

    text = models.TextField(u'HTML для вставки',
                            blank=True,
                            null=True,
                            help_text=u'Оставить поле пустым, если используется картинка')
    
    visible = models.BooleanField(u'Отображать на сайте', default=True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        ordering = 'name',
        verbose_name = u'банер'
        verbose_name_plural = u'Банеры'


# class HeadBanner(models.Model):
#     name = models.CharField(u'Имя банера', max_length=255)
#
#     link = models.URLField(u'Ссылка', blank=True, null=True)
#
#     file = models.ImageField(u'Картинка',
#         upload_to = lambda instance, filename: getmd5(u'ads/background', filename),
#         blank=True, null=True, help_text=u"Используется только для банера в шапке")
#
#     visible = models.BooleanField(u'Отображать на сайте', default=True)
#
#     def __unicode__(self):
#         return self.name
#
#     class Meta:
#         ordering = 'name',
#         verbose_name = u'банер в шапке'
#         verbose_name_plural = u'Банеры в шапке'
