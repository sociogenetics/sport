# -*- coding: utf-8 -*-
from django import template
from django.shortcuts import get_object_or_404
from xendor.thumbnail import thumbnail

from ..models import BanerZone, Banner

register = template.Library()

@register.inclusion_tag('ads/tags/banner.html', takes_context=True)
def ads_banner(context, id):
    zone = get_object_or_404(BanerZone, pk=id)
    banners = Banner.objects.filter(baner_zone=zone, visible=True).order_by('?')
    image = ''
    if banners.count() > 0:
        baner = banners[0]
        image = thumbnail(str(baner.file), '%s;%s;fix' % (zone.width.replace('px', ''), zone.height.replace('px', '')))
    else:
        baner = None
    
    return {
        'zone': zone,
        'baner': baner,
        'image': image,
    }

