# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField

from xendor.utils import getmd5
from xendor.utils import generate_slug

         
class News(models.Model):
    """Новость"""

    title = models.CharField(u'Заголовок новости', max_length=245)
    created = models.DateField(u'Дата публикации', null=True, blank=True)
    # anons = models.TextField(u'Анонс')
    text = HTMLField(u'Текст новости')
    visible = models.BooleanField(u'Опубликовано', default=False)

    # slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    # image = models.ImageField(u'Главное изображение',
    #     upload_to = lambda instance, filename: getmd5('news', filename),
    #     blank=True)

    #Метатеги
    # meta_title = models.CharField(u'Мета заголовок категории', max_length=255, help_text=u'Метатег title', blank=True)
    # meta_description = models.TextField(u'Мета описание категории', help_text=u'Метатег description', blank=True)
    # meta_keywords = models.TextField(u'Мета описание категории', help_text=u'Метатег description', blank=True)

    # def save(self, *args, **kwargs):
    #     self.slug = generate_slug(self, self.slug or self.title)
    #     super(News, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    # @models.permalink
    # def get_absolute_url(self):
    #     return ('news-item', [self.slug,])


    class Meta:
        ordering = '-created',
        verbose_name = u'новость'
        verbose_name_plural = u'Новости'