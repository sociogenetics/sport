# -*- coding: utf-8 -*-
from django.contrib import admin

from xendor.admin_utils import image_field

from .models import News


class NewsAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    search_fields = ('title',)
    list_display = ( 'title', 'created', 'visible')
    list_filter = ('visible',)
    list_display_links = ('title', 'created',)
    list_editable = ('visible',)

    admin_label = u'Новости'

    # poster = image_field('image', u'Изображение', u'100;200')


admin.site.register(News, NewsAdmin)