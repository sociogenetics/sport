# -*- coding: utf-8 -*-
from django import template

from ..models import News

from xendor.settings import XendorSettings

register = template.Library()

@register.inclusion_tag('news/tags/last.html')
def news_last():
    """Последние новости"""

    return {'news_list': News.objects.order_by('-created')[:3]}



