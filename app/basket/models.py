# -*- coding: utf-8 -*-
from django.contrib.contenttypes import generic
from django.db import models
from django.contrib.contenttypes.models import ContentType


class OrderedItem(models.Model):
    """Заказанный объект"""

    #Привязка к контент-тайп фреймворку
    content_type = models.ForeignKey(ContentType, verbose_name=u'Тип контента', related_name="content_type_set_for_%(class)s")
    object_pk = models.IntegerField('ID объекта')
    content_object = generic.GenericForeignKey(ct_field="content_type", fk_field="object_pk")

    #На случай будущих изменений цены
    price = models.DecimalField(u'Цена на момент заказа', max_digits=19, decimal_places=2, null=True, blank=True)
    count = models.IntegerField(u'Количество', default=1)
    order = models.ForeignKey('Order', verbose_name=u'Заказ')

    class Meta:
        verbose_name = u'заказанный объект'
        verbose_name_plural = u'Заказанные объекты'

    def __unicode__(self):
        return unicode(self.content_type.get_object_for_this_type(pk=self.object_pk).name)

    def get_total_price(self):
        return self.count * self.price


class Delivery(models.Model):
    """Справочник доставки"""

    name = models.CharField(u'Название', max_length=255)
    price = models.DecimalField(u'Стоимость', max_digits=19, decimal_places=2, default=0, help_text=u'Фиксированная сумма в рублях')

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = u'Справочник доставки'
        verbose_name_plural = u'Справочник доставки'


class Payment(models.Model):
    """Способ оплаты"""
    name = models.CharField(u'Название', max_length=255)
    price = models.IntegerField(u'Стоимость %', default=0, help_text=u'Стоимость доставки в процентах')

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = u'Способ оплаты'
        verbose_name_plural = u'Способ оплаты'


class OrderStatus(models.Model):
    """Статусы заказа"""

    name = models.CharField(u'Название статуса', max_length=255)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = u'статус'
        verbose_name_plural = u'Статусы'


class Order(models.Model):
    """Заказы с сайта"""

    created = models.DateTimeField(u'Дата оформления заказа', auto_now_add=True)
    price = models.DecimalField(u'Сумма заказа', max_digits=19, decimal_places=2, null=True, blank=True)

    delivery = models.ForeignKey(Delivery, verbose_name=u'Доставка', null=True, blank=True)
    payment = models.ForeignKey(Payment, verbose_name=u'Способ оплаты', null=True, blank=True)
    person = models.CharField(u'Ваше имя', max_length=255, null=True, blank=True)
    phone = models.CharField(u'Ваш номер телефона', max_length=255)
    mail = models.EmailField(u'Почта', max_length=255, blank=True, null=True)
    address = models.TextField(u'Адрес доставки', blank=True, null=True)
    comment = models.TextField(u'Комментарий', blank=True, null=True)
    eula = models.BooleanField(u'Подтверждение ознакомления с пользовательским соглашением', default=False)

    status = models.ForeignKey(OrderStatus, verbose_name=u'Статус заказа', null=True, blank=True)
    order_comment = models.TextField(u'Комментарий менеджера', blank=True, null=True)

    def __unicode__(self):
        return str(self.created)

    class Meta:
        ordering = u'-created',
        verbose_name = u'заказ с сайта'
        verbose_name_plural = u'Заказы с сайта'