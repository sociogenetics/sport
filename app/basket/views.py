# -*- coding: utf-8 -*-
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import get_current_site
from django.core.mail import EmailMessage
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.views.generic import CreateView, TemplateView, RedirectView
from xendor.cbv import RefererRedirectWithMessage, ContentTypeMixin, ToStructureMixin, JSONResponseMixin, WithMailer

from xendor.settings import XendorSettings
from xendor.templatetags.utils import _formater_1000
from app.basket import clean_basket
from app.basket.forms import OrderForm
from app.basket.models import Order, OrderedItem, Delivery, Payment


def _recalculate_basket_price(basket):
    """Пересчет суммарной цены, планируемые скидки и прочую фигню подключать здесь"""

    # ContentType.objects.get_for_id().get_object_for_this_type(pk=pk)

    return sum([ContentType.
                objects.
                get_for_id(basket[x]['content_type_id']).
                get_object_for_this_type(pk=basket[x]['object_id']).
                get_price() * basket[x]['count']
                for x in basket.keys() if x not in ('price', 'payment', 'delivery')])


def _get_basket_items(basket):
    """получение списка товаров лежащих в корзине"""

    return [(ContentType.
                      objects.
                      get_for_id(basket[x]['content_type_id']).
                      get_object_for_this_type(pk=basket[x]['object_id']), basket[x]['count'])
                      for x in basket.keys() if x not in ('price', 'payment', 'delivery')]


class OrderView(WithMailer, CreateView):
    """Заказ товаров"""

    model = Order
    form_class = OrderForm
    template_name = 'basket/order.html'

    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data(**kwargs)
        basket = self.request.session.get('basket', {})
        context.update({
            'items': _get_basket_items(basket),
            'price': basket.get('price', 0),
            'count': len([y for x, y in basket.items() if not x in ('price',)]),
        })
        return context

    def get_success_url(self):
        return reverse('basket-success')

    def form_valid(self, form):
        basket = self.request.session.get('basket', {})

        price = float(basket.get('price', 0))
        delivery_price = 0
        payment_price = 0
        try:
            delivery_price = form.cleaned_data.get('delivery').price
        except:
            pass

        try:
            payment_price = form.cleaned_data.get('payment').price
        except:
            pass

        price = float(price) + float(delivery_price)
        price = price + (float(payment_price) * price/100)

        self.object = form.save()
        self.object.price = price

        items = _get_basket_items(basket)

        for item in items:
            ordered_item = OrderedItem(
                content_object=item[0],
                price=item[0].price,
                count=item[1],
                order=self.object
            )
            ordered_item.save()

            clean_basket(self.request)

        self.send_mail(
            'Новая покупка на сайте {site_name}',
            'basket/order-mail.html',
            {'order': self.object},
            XendorSettings().get(u'E-mail администратора'))

        if self.object.mail:
            self.send_mail(
                'Новая покупка на сайте {site_name}',
                'basket/confirm-mail.html',
                {'order': self.object},
                self.object.mail)

        return super(OrderView, self).form_valid(form)


class AddToBasket(ContentTypeMixin, RefererRedirectWithMessage):
    """добавление одного товара в корзину"""

    action_message = u'Товар добавлен в корзину'

    def action(self):
        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']
        try:
            basket[pk]['count'] += 1
        except KeyError:
            basket[pk] = {
                'count': 1,
                'content_type_id': self.kwargs['content_type_id'],
                'object_id': self.kwargs['object_id']
            }

        basket.update(price=_recalculate_basket_price(basket))
        self.request.session['basket'] = basket

    def get_redirect_url(self, *args, **kwargs):
        return super(AddToBasket, self).get_redirect_url(*args, **kwargs) + '#animate-basket'


class RemoveFromBasket(ContentTypeMixin, RefererRedirectWithMessage):
    """Удаление товара из корзины"""

    action_message = u'Товар удален из корзины'

    def action(self):
        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']

        del basket[pk]

        basket.update(price=_recalculate_basket_price(basket))
        self.request.session['basket'] = basket


class RemoveOneFromBasket(ContentTypeMixin, RefererRedirectWithMessage):
    """Удаление товара из корзины"""

    action_message = u'Товар удален из корзины'

    def action(self):
        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']

        if basket[pk]['count'] == 1:
            del basket[pk]
        else:
            basket[pk]['count'] -= 1

        basket.update(price=_recalculate_basket_price(basket))
        self.request.session['basket'] = basket


class AddToBasketJSON(ContentTypeMixin, JSONResponseMixin, TemplateView):
    """добавление одного товара в корзину"""

    def get_context_data(self, **kwargs):

        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']
        try:
            basket[pk]['count'] += 1
        except KeyError:
            basket[pk] = {
                'count': 1,
                'content_type_id': self.kwargs['content_type_id'],
                'object_id': self.kwargs['object_id']
            }

        price = _recalculate_basket_price(basket)
        basket.update(price=price)
        self.request.session['basket'] = basket

        return {'price': _formater_1000(str(price)), 'count': basket[pk]['count']}


class RemoveFromBasketJSON(ContentTypeMixin, JSONResponseMixin, TemplateView):
    """Удаление товара из корзины"""

    action_message = u'Товар удален из корзины'

    def get_context_data(self, **kwargs):
        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']

        del basket[pk]

        price = _recalculate_basket_price(basket)
        basket.update(price=price)
        self.request.session['basket'] = basket

        return {'price': _formater_1000(str(price)), 'count': 0}


class RemoveOneFromBasketJSON(ContentTypeMixin, JSONResponseMixin, TemplateView):
    """Удаление товара из корзины"""

    action_message = u'Товар удален из корзины'

    def get_context_data(self, **kwargs):
        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']

        try:
            if basket[pk]['count'] == 1:
                del basket[pk]
                count = 0
            else:
                basket[pk]['count'] -= 1
                count = basket[pk]['count']
        except:
            pass


        price = _recalculate_basket_price(basket)
        basket.update(price=price)
        self.request.session['basket'] = basket

        return {'price': _formater_1000(str(price)), 'count': count}


class SetCountBasket(ContentTypeMixin, JSONResponseMixin, TemplateView):
    """Установка определенного количества товаров в корзине"""

    http_method_names = [u'get']

    template_name = ''

    def get_context_data(self, **kwargs):

        basket = self.request.session.get('basket', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']
        count = int(self.request.GET.get('count', 1))

        try:
            basket[pk]['count'] = count
        except KeyError:
            basket[pk] = {
                'count': count,
                'content_type_id': self.kwargs['content_type_id'],
                'object_id': self.kwargs['object_id']
            }

        price = _recalculate_basket_price(basket)
        basket.update(price=price)
        self.request.session['basket'] = basket

        return {'price': _formater_1000(str(price))}


class GetDelivery(JSONResponseMixin, TemplateView):
    """Получения значения стоимости доставки"""

    http_method_names = [u'get']

    template_name = ''

    def get_context_data(self, **kwargs):

        basket = self.request.session.get('basket', {})

        price = float(basket.get('price'))
        delivery_price = 0
        payment_price = 0
        if self.request.GET.get('delivery'):
            delivery = get_object_or_404(Delivery, pk=self.request.GET.get('delivery'))
            delivery_price = float(delivery.price)

        if self.request.GET.get('payment'):
            payment = get_object_or_404(Payment, pk=self.request.GET.get('payment'))
            payment_price = float(payment.price)

        price = float(price) + float(delivery_price)
        price = price + (float(payment_price) * price/100)

        return {
            'price': _formater_1000(str(price)),
            'deliveryCost': delivery_price,
            'paymentCost': payment_price
        }

