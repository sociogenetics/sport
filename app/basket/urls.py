# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.views.generic import TemplateView
from app.basket import views

from app.basket.views import AddToBasket, RemoveOneFromBasket, RemoveFromBasket, OrderView, SetCountBasket, \
    AddToBasketJSON, RemoveFromBasketJSON, RemoveOneFromBasketJSON, GetDelivery

urlpatterns = patterns('',
    ###==оформление заказа==###
    #Список товаров корзины
    #url(r'^$', Index, name = 'basket-index'),

    #форма заказа
    url(r'^order/$', OrderView.as_view(), name = 'basket-order'),

    #Окончательное завершение заказа
    url(r'^success/$', TemplateView.as_view(template_name='basket/success.html'), name = 'basket-success'),

    #Удаление товара из корзины
    url(r'^remove/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', RemoveFromBasket.as_view(), name = 'basket-remove'),
    url(r'^remove-json/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', RemoveFromBasketJSON.as_view(), name = 'basket-remove-json'),

    #Удаление одного товара из корзины
    url(r'^remove-one/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', RemoveOneFromBasket.as_view(), name = 'basket-remove-one'),
    url(r'^remove-one-json/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', RemoveOneFromBasketJSON.as_view(), name = 'basket-remove-one-json'),

    url(r'^add/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', AddToBasket.as_view(), name='basket-add'),
    url(r'^add-json/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', AddToBasketJSON.as_view(), name='basket-add-json'),

    url(r'^set-count/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', SetCountBasket.as_view(), name='basket-set-count'),


    url(r'^get-delivery/$', GetDelivery.as_view(), name = 'basket-get-delivery'),

)