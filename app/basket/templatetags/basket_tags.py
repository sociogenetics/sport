# -*- coding: utf-8 -*-
from django import template
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from app.basket.views import _get_basket_items

register = template.Library()

@register.inclusion_tag('basket/tags/informer.html', takes_context=True)
def basket_informer(context):
    """Информер корзины"""

    basket = context['request'].session.get('basket', {})

    return {
        'count': len([y for x, y in basket.items() if not x in ('price',)]),
        'price': basket.get('price', 0),
        'items': _get_basket_items(basket)
    }


@register.assignment_tag
def basket_url(url_pattern, content_object):
    """рендер урла для ссылки с использованием контент-объекта"""

    content_type = ContentType.objects.get_for_model(content_object.__class__)
    return reverse(url_pattern, args=[content_type.pk, content_object.pk])


@register.simple_tag
def basket_url_link(url_pattern, content_object):
    """рендер урла для ссылки с использованием контент-объекта"""

    content_type = ContentType.objects.get_for_model(content_object.__class__)
    return reverse(url_pattern, args=[content_type.pk, content_object.pk])




