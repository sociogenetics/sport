# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from xendor.thumbnail import thumbnail

from app.basket.models import Order, OrderedItem, Delivery, Payment, OrderStatus


class OrderItemInline(admin.TabularInline):
    model = OrderedItem
    extra = 0
    fields = ('preview', 'product', 'price', 'count')
    readonly_fields = ('preview', 'product', 'price', 'count')

    def product(self, instance):
        return '<a href="%s" target="_blank">%s</a>' % ('/admin/catalog/product/%s/' % instance.object_pk, instance)

    product.short_description = "Товар"
    product.allow_tags = True

    def preview(self, instance):
        obj = instance.content_type.get_object_for_this_type(pk=instance.object_pk)
        image_path = thumbnail(str(obj.image), '150;100')
        return '<a href="%s" target="_blank"><img src="%s"></a>' % ('/admin/catalog/product/%s/' % instance.object_pk, image_path)

    preview.short_description = "Изображение"
    preview.allow_tags = True

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj):
        return False


class OrderAdmin(admin.ModelAdmin):
    list_display = ('pk', 'created', 'price', 'status', 'order_comment')
    list_editable = ('status', 'order_comment')
    list_filter = ('status', )
    list_display_links = ('pk', 'created')
    # date_hierarchy = 'created'
    actions = None
    inlines = [OrderItemInline]

    def has_add_permission(self, request):
        return False


admin.site.register(Order, OrderAdmin)


class DeliveryAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')


admin.site.register(Delivery, DeliveryAdmin)


class PaymentAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')

admin.site.register(Payment, PaymentAdmin)

admin.site.register(OrderStatus)