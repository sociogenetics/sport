# -*- coding: utf-8 -*-
from django import forms
from app.basket.models import Order
from django.core.exceptions import ValidationError


class OrderForm(forms.ModelForm):
    
    class Meta:
        model = Order
        widgets = {
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'delivery': forms.Select(attrs={'class': 'form-control delivery-calculation'}),
            'payment': forms.Select(attrs={'class': 'form-control payment-calculation'}),
            'person': forms.TextInput(attrs={'class': 'form-control'}),
            'mail': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
            'comment': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
        }
        exclude = ('price', 'status', 'order_comment')