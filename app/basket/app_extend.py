# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from xendor.structure import Structure

Structure().register_app(
    app_id='basket',
    app_name=u'Корзина',
    node_url=lambda: reverse('basket-order'),
    children=[
        {
            'title': 'Поздравляем с покупкой!',
            'url': lambda: reverse('basket-success'),
            'children': [],
            'in_menu': True,
            'parameters': {},
            'meta_title': None,
            'meta_description': None
        },
    ],
    node_parameters={},
    safe_for_structure=False
)