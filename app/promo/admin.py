# -*- coding: utf-8 -*-
from django.contrib import admin

from xendor.admin_utils import image_field

from app.promo.models import Block, Images


class BlockImageInline(admin.TabularInline):
    model = Images
    extra = 0


class BlockAdmin(admin.ModelAdmin):

    list_display = ('title', 'link_text', 'order', 'visible', )
    list_filter = ('visible',)
    list_editable = ('order', 'visible',)

    inlines = [
        BlockImageInline,
    ]

    #poster = image_field('image', u'Изображение', u'100x100')

    admin_label = u'Промоблок на главной'

admin.site.register(Block, BlockAdmin)