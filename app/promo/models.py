# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField
from xendor.utils import getmd5

class Block(models.Model):
    """Блок промоблока"""

    title = models.CharField(u'Заголовок', max_length=255)
    link_text = models.CharField(u'Текст ссылки', max_length=255, blank=True, null=True, editable=False)
    link_url = models.URLField(u'Url ссылки', max_length=255, blank=True, null=True)
    image = models.ImageField(u'Изображение', upload_to = lambda instance, filename: getmd5('promo', filename), blank=True, null=True, editable=False)
    text = HTMLField(u'Текст', blank=True, editable=False)
    order = models.IntegerField(u'Порядок', help_text=u'Чем меньше число тем выше приоритет вывода пунктов', default=350)
    visible = models.BooleanField(u'Опубликовано', default=True)


    def __unicode__(self):
        return self.title

    class Meta:
        ordering = u'order',
        verbose_name = u'блок'
        verbose_name_plural = u'Блоки'


class Images(models.Model):
    """Картинки для параллакса"""

    block = models.ForeignKey(Block, verbose_name=u'Блок')
    image = models.ImageField(u'Изображение', upload_to=lambda instance, filename: getmd5('promo/img', filename))
    order = models.IntegerField(u'Порядок', help_text=u'Чем меньше число тем выше приоритет вывода пунктов', default=350)
    depth = models.CharField(u'Глубина', max_length=100, help_text=u'от 0.00 до 1.00', )
