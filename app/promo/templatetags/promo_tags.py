# -*- coding: utf-8 -*-
from django import template
from xendor.settings import XendorSettings

from app.promo.models import Block

register = template.Library()

@register.inclusion_tag('promo/block.html')
def promo_block():
    """промоблок"""

    return {'blocks': Block.objects.filter(visible=True)}


@register.inclusion_tag('promo/countdown.html')
def countdown():
    """Часики осталось до доставки"""

    days = sorted(XendorSettings().get(u'Порядковый номер дня доставки').split(','))
    time = XendorSettings().get(u'Время сброса счетчика')

    import datetime
    now = datetime.datetime.utcnow()

    hour, minutes = time.split(':')
    time = datetime.time(int(hour), int(minutes))

    depart = None

    for day in days:
        delta = int(day) - int(now.weekday())
        if delta == 0:
            if datetime.time(hour=time.hour, minute=time.minute) > now.time():
                depart = datetime.datetime(now.year, now.month, now.day, time.hour, time.minute) - now
                break
        if delta > 0:
            depart = datetime.timedelta(days=delta, hours=time.hour - now.hour, minutes=time.minute - now.minute)
            break

    if depart is None:
        for day in days:
            day = int(day) + 7
            delta = int(day) - int(now.weekday())
            if delta == 0:
                if datetime.time(hour=time.hour, minute=time.minute) > now.time():
                    depart = datetime.datetime(now.year, now.month, now.day, time.hour, time.minute) - now
                    break
            if delta > 0:
                depart = datetime.timedelta(days=delta, hours=time.hour - now.hour, minutes=time.minute - now.minute)
                break

    try:
        hours, minutes, secondes = unicode(depart).split(',')[1].strip().split(':')
    except IndexError:
        hours, minutes, secondes = unicode(depart).split(',')[0].strip().split(':')

    return {
        'days': depart.days,
        'hours': hours,
        'minutes': minutes,
    }