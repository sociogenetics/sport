# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Images'
        db.create_table(u'promo_images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['promo.Block'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=350)),
            ('depth', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'promo', ['Images'])


        # Changing field 'Block.text'
        db.alter_column(u'promo_block', 'text', self.gf('tinymce.models.HTMLField')())

        # Changing field 'Block.link_url'
        db.alter_column(u'promo_block', 'link_url', self.gf('django.db.models.fields.URLField')(max_length=255, null=True))

    def backwards(self, orm):
        # Deleting model 'Images'
        db.delete_table(u'promo_images')


        # Changing field 'Block.text'
        db.alter_column(u'promo_block', 'text', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Block.link_url'
        db.alter_column(u'promo_block', 'link_url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    models = {
        u'promo.block': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Block'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '350'}),
            'text': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'promo.images': {
            'Meta': {'object_name': 'Images'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['promo.Block']"}),
            'depth': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '350'})
        }
    }

    complete_apps = ['promo']