# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView

from xendor.cbv import ToStructureMixin, PaginatedListMixin

from .models import Article


class Index(PaginatedListMixin, ListView):
    """Индекс"""

    model = Article
    queryset = Article.objects.filter(visible=True).order_by('-created')
    template_name = 'article/index.html'
    paginate_by = 12


class Item(ToStructureMixin, DetailView):
    """Вьюшка публикации"""

    model = Article
    meta_title = lambda o: o.get_object().meta_title or o.get_object().title
    meta_description = lambda o: o.get_object().meta_description or o.get_object().title
    meta_keywords = lambda o: o.get_object().meta_keywords or o.get_object().title
    breadcrumbs = lambda o: o.get_object().title
    activated_node = lambda o: reverse('article-index')
    template_name = 'article/item.html'