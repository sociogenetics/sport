# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField

from xendor.utils import getmd5
from xendor.utils import generate_slug

from app.catalog.models import Product


class Article(models.Model):
    """публикация"""

    title = models.CharField(u'Заголовок', max_length=255)

    description = models.TextField(u'Краткое описание')
    text = HTMLField(u'Текст публикации')
    visible = models.BooleanField(u'Опубликовано', default=False)
    created = models.DateTimeField(u'Дата публикации', auto_now_add=True)

    image = models.ImageField(u'Изображение',
        upload_to = lambda instance, filename: getmd5('article', filename),
        blank=True)

    slug = models.SlugField(u'Слаг', max_length=150, null=True, unique=True, blank =True)

    products = models.ManyToManyField(Product, verbose_name=u'Ассоциированные товары')

    #Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова', help_text=u'Метатег keywords', blank=True)

    def __unicode__(self):
        return self.title

    def get_products(self):
        return self.products.order_by('?')[:4]

    @models.permalink
    def get_absolute_url(self):
        return ('article-item', [self.slug,])

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.title)
        super(Article, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'публикация'
        verbose_name_plural = u'Публикации'
