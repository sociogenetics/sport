# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='article',
    app_name=u'Публикации',
    node_url=lambda: reverse('article-index'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)



