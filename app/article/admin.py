# -*- coding: utf-8 -*-
from django.contrib import admin

from xendor.admin_utils import image_field

from .models import Article


class ArticleAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('poster', 'title', 'created', 'visible')
    list_filter = ('visible',)
    list_display_links = ('title', 'created',)
    list_editable = ('visible',)
    filter_horizontal = 'products',
    admin_label = u'Публикации'

    poster = image_field('image', u'Изображение', u'100;100')

admin.site.register(Article, ArticleAdmin)