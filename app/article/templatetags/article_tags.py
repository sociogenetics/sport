# -*- coding: utf-8 -*-
from django import template

from ..models import Article
from app.catalog.models import Product

from xendor.settings import XendorSettings

register = template.Library()

@register.inclusion_tag('article/tags/by-topic.html')
def article_by_category(category):
    """популярные категории"""

    return {
        'articles': Article.objects.filter(
            pk__in=Product.objects.filter(
                category__in=category.get_descendants(include_self=True)
            ).exclude(article=None).order_by('article').values_list('article').distinct()
        ).order_by('?')[:3]
    }

