# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView

from xendor.cbv import ToStructureMixin, WithMailer
from xendor.settings import XendorSettings

from .models import Order


class IndexView(ToStructureMixin, WithMailer, CreateView):
    """Главная (index)"""

    model = Order
    meta_title = meta_description = breadcrumbs = u'Заказать звонок'
    success_url = '/'

    def get_template_names(self):
        if self.request.is_ajax():
            return ['callback/ajax.html']

        return super(IndexView, self).get_template_names()

    def get_success_url(self):
        if self.request.is_ajax():
            return reverse('callback-success')

        return super(IndexView, self).get_success_url()
    
    def form_valid(self, form):
        
        self.send_mail(
            'Заявка на обратный звонок на сайте {site_name}',
            'callback/order-mail.html',
            form.cleaned_data,
            XendorSettings().get(u'E-mail администратора'))

        form.instance.save()
        
        from django.contrib import messages
        messages.add_message(self.request, messages.SUCCESS, 'Спасибо! Заявка отправлена администрации сайта.')
        return super(IndexView, self).form_valid(form)
