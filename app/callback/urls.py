# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.generic.base import TemplateView

from .views import IndexView

urlpatterns = patterns('',
    #Индекс
    url(r'^$', IndexView.as_view(), name = 'callback-index'),

    #Сообщение об успешном отсыле заявки
    url(r'^success/$', TemplateView.as_view(template_name='callback/ajax-success.html'), name = 'callback-success'),
)