# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='callback',
    app_name=u'Заказать звонок',
    node_url=lambda: reverse('callback-index'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)

