# -*- coding: utf-8 -*-
from django import template

from ..forms import callbackForm

register = template.Library()

@register.inclusion_tag('callback/tags/control.html', takes_context=True)
def callback_control(context):
    """Кнопка вызова окошка перезвонилки"""

    return context

@register.inclusion_tag('callback/tags/window.html', takes_context=True)
def callback_window(context):
    """Окошко перезвонилки"""

    context.update({'callback_form': callbackForm(),})
    return context