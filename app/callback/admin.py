# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Order

class callbackAdmin(admin.ModelAdmin):
    list_display = ('person', 'phone', 'created', )
    admin_label = u'Заказать звонок'
    
admin.site.register(Order, callbackAdmin)