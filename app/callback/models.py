# -*- coding: utf-8 -*-
from django.db import models


class Order(models.Model):
    """Заявки на перезвонилку"""

    person = models.CharField(u'Представьтесь', max_length=255)
    phone = models.CharField(u'Телефон', max_length=255)
    created = models.DateTimeField('Создано', auto_now_add=True, null=True)

    def __unicode__(self):
        return unicode(self.created)

    class Meta:
        ordering = '-id',
        verbose_name = u'заявка на перезвонилку'
        verbose_name_plural = u'Заявки на перезвонилки'
