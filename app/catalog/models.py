# -*- coding: utf-8 -*-
from datetime import datetime

from django.core.mail import EmailMessage
from django.db import models
from django import forms
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.loader import render_to_string

from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from tinymce.models import HTMLField
from xendor.settings import XendorSettings

from xendor.utils import generate_slug, getmd5


class Category(MPTTModel):
    """Категории товаров"""

    parent = TreeForeignKey(u'self', verbose_name=u'Родитель', null=True, blank=True, related_name=u'children')

    name = models.CharField(verbose_name=u"Название", max_length=240, null=False, blank=False)
    header = models.CharField(verbose_name=u"Заголовок", max_length=240, null=True, blank=True)

    description = HTMLField(u'Описание категории', blank=True, null=True)

    image = models.ImageField(u'Изображение для категории',
        upload_to=lambda instance, filename: getmd5(u'catalog/category', filename),
        blank=True, null=True)

    slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    visible = models.BooleanField(u'Отображать на сайте', default=True)
    special = models.BooleanField(u'Выводить в спецпредложениях на главной', default=False)
    template = models.CharField(u'Шаблон списка', max_length=255, choices=(('double', u'Два столбца'), ('single', u'Один столбец'),), default=u'single')

    #Метатеги
    meta_title = models.CharField(u'Мета заголовок категории', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова', help_text=u'Метатег keywords', blank=True)

    #Необходимые для переноса данные
    old_id = models.CharField(u'Старый айдишник', max_length=255, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = self.slug = generate_slug(self, self.slug or self.name)
        super(Category, self).save(*args, **kwargs)

    class MPTTMeta:
        pass

    @models.permalink
    def get_absolute_url(self):
        if self.is_leaf_node():
            return ('catalog-subcategory', [self.slug])
        else:
            return ('catalog-category', [self.slug])

    def get_header(self):
        return self.header or self.name

    def __unicode__(self):
        if self.parent:
            return unicode(self.parent) + u' / ' + unicode(self.name)
        return unicode(self.name)

    class Meta:
        verbose_name = u'категория'
        verbose_name_plural = u'Категории'


class PopularCategory(models.Model):
    """Популярные категории (выводятся на главной)"""

    name = models.CharField(verbose_name=u"Название", max_length=240, null=False, blank=False)
    image = models.ImageField(u'Изображение',
        upload_to=lambda instance, filename: getmd5(u'catalog/popular', filename),
        blank=True, null=True)

    categories = models.ManyToManyField(Category, verbose_name=u'Список категорий')

    order = models.IntegerField(u'Порядок', help_text=u'Чем меньше число тем выше приоритет вывода пунктов', default=350)
    visible = models.BooleanField(u'Отображать на сайте', default=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        ordering = 'order',
        verbose_name = u'категория'
        verbose_name_plural = u'Популярные категории'


PARAMETER_TYPES = (
    (u'Текстовое поле', 'text'),
)


class Parameter(models.Model):
    """Параметры товара (типы)"""

    category = models.ForeignKey(Category, verbose_name=u'Категория')
    name = models.CharField(u'Название параметра', max_length=255)
    values = models.TextField(u'Допустимые значения')
    order = models.IntegerField(u'Порядок', help_text=u'Чем меньше число тем выше приоритет вывода пунктов', default=350)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        ordering = 'order',
        verbose_name = u'Параметр товара'
        verbose_name_plural = u'Параметры товара'


class Brand(models.Model):
    """Торговые марки (Бренды)"""

    name = models.CharField(u'Название торговой марки', max_length=240)
    slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    image = models.ImageField(u'Логотип',
        upload_to=lambda instance, filename: getmd5(u'catalog/brand', filename),
        blank=True, null=True)

    description = HTMLField(u'Описание торговой марки', blank=True, null=True)

    #Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова', help_text=u'Метатег keywords', blank=True)

    def save(self, *args, **kwargs):
        self.slug = self.slug = generate_slug(self, self.slug or self.name)
        super(Brand, self).save(*args, **kwargs)

    # @models.permalink
    # def get_absolute_url(self):
    #     return ('catalog-brand', [self.slug])

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = u'бренд'
        verbose_name_plural = u'Бренды'


PRODUCT_DELIVERY_STATUS = (
    ('in_stock', u'В наличии'),
    ('orderable', u'Под заказ'),
    ('out_stock', u'Отсутствует'),
)


class Product(models.Model):
    """Модель товара"""

    category = models.ManyToManyField(Category, verbose_name=u'Категория', null=True, blank=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Бренд', null=True, blank=True)
    name = models.CharField(u'Название товара', max_length=240, blank=True, null=True)
    xls_import_name = models.CharField(verbose_name=u"Название товара в прайсе", max_length=240, blank=True, default=u'')
    short_description = HTMLField(u'Краткое описание товара', blank=True, null=True)
    description = HTMLField(u'Описание товара', blank=True, null=True)
    comparison_text = HTMLField(u'Описание для сравнения', blank=True, null=True)
    slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    image = models.ImageField(u'Главное изображение',
        upload_to=lambda instance, filename: getmd5(u'catalog/main', filename),
        blank=True, null=True)

    visible = models.BooleanField(u'Опубликовано', default=True)

    delivery_status = models.CharField(u'Наличие', max_length=255, choices=PRODUCT_DELIVERY_STATUS, default='in_stock')

    old_price = models.DecimalField(u'Старая цена', max_digits=19, decimal_places=2, default=0)
    price = models.DecimalField(u'Цена', max_digits=19, decimal_places=2, default=0)

    parameters = models.ManyToManyField(Parameter, through='ProductToParameters')
    is_new = models.BooleanField(u'Новинка', default=False)

    product_group = models.ManyToManyField('self', verbose_name=u'Товарная группа', max_length=255, blank=True, null=True)
    check_by_price = models.BooleanField(u'Наличие по прайсу', default=False)
    to_yml = models.BooleanField(u'Выгружать в Y.Маркет', default=False)


    #Метатеги
    meta_title = models.CharField(u'Мета заголовок категории', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание категории', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова', help_text=u'Метатег keywords', blank=True)

    #Необходимые для переноса данные
    old_id = models.CharField(u'Старый айдишник', max_length=255, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.name)
        super(Product, self).save(*args, **kwargs)

    def get_photos(self):
        photos = []

        if self.image:
            photos.append(self.image)

        for item in self.photo_set.all():
            photos.append(item.image)

        return photos

    @models.permalink
    def get_absolute_url(self):
        return ('catalog-product', [self.slug])

    def get_price(self):
        return float(self.price)

    def get_picture(self):
        try:
            return self.image.url
        except: pass

    def get_category(self):
        try:
            return self.category.all()[0]
        except: pass

    def get_ymarket_available(self):
        return (self.delivery_status == 'in_stock') and 'true' or 'false'

    def get_vendor(self):
        if self.brand:
            return self.brand.name

    def get_model(self):
        return self.name

    def get_description(self):
        return self.short_description

    def get_resposes(self):
        return self.response_set.filter(visible=True)

    class Meta:
        verbose_name = u'товар'
        verbose_name_plural = u'Товары'


@receiver(pre_save, sender=Product)
def update_status_signal(sender, **kwargs):
    obj = kwargs['instance']
    if obj.pk:
        in_db_obj = Product.objects.get(pk=obj.pk)
        if (obj.delivery_status == 'in_stock') and (in_db_obj.delivery_status == 'out_stock'):
            #send reminder letters
            for reminder in in_db_obj.productreminder_set.filter(is_send=False):
                mail_from = XendorSettings().get(u'Обратный e-mail')
                email_message = EmailMessage(
                    'Товар доступен для покупки',
                    render_to_string('catalog/reminder-mail.html', {
                        'product': obj
                    }), mail_from, [reminder.mail])
                email_message.content_subtype = 'html'
                email_message.send()

                reminder.is_send = True
                reminder.send_data = datetime.now()
                reminder.save()


class ProductReminder(models.Model):
    """подписка на появление товара в продаже"""

    product = models.ForeignKey(Product, verbose_name=u'Товар')
    mail = models.EmailField(u'E-mail')
    created = models.DateTimeField(u'Создано', auto_now_add=True)
    is_send = models.BooleanField(u'Извещение отправлено', default=False)
    send_data = models.DateTimeField(u'Дата отправки извещения', blank=True, null=True)

    def __unicode__(self):
        return unicode(self.mail)

    class Meta:
        verbose_name = u'Подписка на товар'
        verbose_name_plural = u'Подписки на товар'


class ReminderForm(forms.ModelForm):

    class Meta:
        model = ProductReminder
        widgets = {
            'product': forms.HiddenInput()
        }
        exclude = 'is_send', 'send_data'


class ProductToParameters(models.Model):
    """Дополнительные параметры товара (значения)"""

    parameter = models.ForeignKey(Parameter, verbose_name=u'Параметр')
    product = models.ForeignKey(Product, verbose_name=u'Товар')
    value = models.TextField(u'Значение', blank=True, null=True)

    def __unicode__(self):
        return unicode(self.parameter)

    class Meta:
        verbose_name = u'Параметры товара'
        verbose_name_plural = u'Параметры товара'


class Photo(models.Model):
    """Изображение"""

    product = models.ForeignKey(Product, verbose_name=u'Товар')
    title = models.CharField(u'Заголовок изображения', max_length = 245, null=True, blank=True, default='')
    image = models.ImageField(u'Изображение',
        upload_to = lambda instance, filename: getmd5('catalog', filename),
        blank=True)

    def __unicode__(self):
        return unicode(self.product) + ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'


#####
# модельки для парсинга старой базы
#####

# class PhpshopCategories(models.Model):
#     id = models.IntegerField(primary_key=True)
#     name = models.CharField(max_length=64)
#     num = models.IntegerField()
#     parent_to = models.IntegerField()
#     yml = models.CharField(max_length=1)
#     num_row = models.CharField(max_length=1)
#     num_cow = models.IntegerField()
#     sort = models.TextField()
#     content = models.TextField()
#     vid = models.CharField(max_length=1)
#     name_rambler = models.CharField(max_length=255)
#     servers = models.CharField(max_length=255)
#     title = models.CharField(max_length=255)
#     title_enabled = models.CharField(max_length=1)
#     title_shablon = models.CharField(max_length=255)
#     descrip = models.CharField(max_length=255)
#     descrip_enabled = models.CharField(max_length=1)
#     descrip_shablon = models.CharField(max_length=255)
#     keywords = models.CharField(max_length=255)
#     keywords_enabled = models.CharField(max_length=1)
#     keywords_shablon = models.CharField(max_length=255)
#     skin = models.CharField(max_length=64)
#     skin_enabled = models.CharField(max_length=1)
#     order_by = models.CharField(max_length=1)
#     order_to = models.CharField(max_length=1)
#     secure_groups = models.CharField(max_length=255)
#
#     class Meta:
#         # managed = False
#         db_table = 'phpshop_categories'
#
#
# class PhpshopProducts(models.Model):
#     id = models.IntegerField(primary_key=True)
#     category = models.IntegerField()
#     name = models.CharField(max_length=255)
#     description = models.TextField()
#     content = models.TextField()
#     price = models.FloatField()
#     price_n = models.FloatField()
#     sklad = models.CharField(max_length=1)
#     p_enabled = models.CharField(max_length=1)
#     enabled = models.CharField(max_length=1)
#     uid = models.CharField(max_length=64)
#     spec = models.CharField(max_length=1)
#     odnotip = models.CharField(max_length=64)
#     vendor = models.CharField(max_length=255)
#     vendor_array = models.TextField()
#     yml = models.CharField(max_length=1)
#     num = models.IntegerField()
#     newtip = models.CharField(max_length=1)
#     title = models.CharField(max_length=255)
#     title_enabled = models.CharField(max_length=1)
#     datas = models.IntegerField()
#     page = models.CharField(max_length=255)
#     user = models.IntegerField()
#     descrip = models.CharField(max_length=255)
#     descrip_enabled = models.CharField(max_length=1)
#     title_shablon = models.CharField(max_length=255)
#     descrip_shablon = models.CharField(max_length=255)
#     keywords = models.CharField(max_length=255)
#     keywords_enabled = models.CharField(max_length=1)
#     keywords_shablon = models.CharField(max_length=255)
#     pic_small = models.CharField(max_length=255)
#     pic_big = models.CharField(max_length=255)
#     yml_bid_array = models.TextField()
#     parent_enabled = models.CharField(max_length=1)
#     parent = models.TextField()
#     items = models.IntegerField()
#     weight = models.FloatField()
#     price2 = models.FloatField()
#     price3 = models.FloatField()
#     price4 = models.FloatField()
#     price5 = models.FloatField()
#     files = models.TextField()
#     baseinputvaluta = models.IntegerField()
#     ed_izm = models.CharField(max_length=255)
#     dop_cat = models.CharField(max_length=255)
#     class Meta:
#         # managed = False
#         db_table = 'phpshop_products'
#
#
# class PhpshopFoto(models.Model):
#     id = models.IntegerField(primary_key=True)
#     parent = models.IntegerField()
#     name = models.CharField(max_length=64)
#     num = models.IntegerField()
#     info = models.CharField(max_length=255)
#     class Meta:
#         # managed = False
#         db_table = 'phpshop_foto'