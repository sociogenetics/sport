# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

from app.catalog.models import Category

Structure().register_app(
    app_id='catalog',
    app_name=u'Каталог магазина',
    node_url=lambda: reverse('catalog-index'),
    children=[
        {
            'title': unicode(i.name),
            'url': i.get_absolute_url,
            'children': [
                {
                    'title': unicode(j.name),
                    'url': j.get_absolute_url,
                    'children': [],
                    'in_menu': True,
                    'parameters': {},
                    'meta_title': unicode(j.meta_title or j.name),
                    'meta_description': unicode(j.meta_description or j.name),
                    'meta_keywords': unicode(j.meta_keywords or j.name),
                }
                for j in Category.objects.filter(parent=i, visible=True)
            ],
            'in_menu': True,
            'parameters': {},
            'meta_title': unicode(i.meta_title or i.name),
            'meta_description': unicode(i.meta_description or i.name),
            'meta_keywords': unicode(i.meta_keywords or i.name),
        }
        for i in Category.objects.filter(level=0, visible=True)
    ],
    node_parameters={},
    safe_for_structure=False
)

Structure().register_app(
    app_id='search',
    app_name=u'Поиск',
    node_url=lambda: reverse('catalog-search'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)

Structure().register_app(
    app_id='comparison',
    app_name=u'Сравнение',
    node_url=lambda: reverse('catalog-comparison'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)
