# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.decorators.cache import cache_page
from django.views.generic.base import TemplateView

from app.catalog.views import Index, CategoryView, ProductView, SubCategoryView, ChangePaginateBy, SearchView, \
    ComparisonView, AddToCompare, RemoveFromCompare, TSportYmarket, BrandView, ProductReminderView, \
    CategoryRedirector, ProductRedirector, CalcFetcher, SendKreditMessage

# from app.catalog.views import ParserView info@t-sport66.ru

urlpatterns = patterns('',

    url(r'^$', Index.as_view(), name='catalog-index'),

    # url(r'^parser/', ParserView.as_view(), name = 'catalog-parser'),
    url(r'^credit-message/', SendKreditMessage.as_view(), name = 'catalog-credit-message'),

    url(r'^shop/CID_(?P<old_pk>\d+)\.html$', CategoryRedirector.as_view(), name = 'catalog-category-redirector'),

    url(r'^shop/UID_(?P<old_pk>\d+)\.html$', ProductRedirector.as_view(), name = 'catalog-product-redirector'),

    url(r'^fetch-calc.html$', CalcFetcher.as_view(), name = 'catalog-calc-fetcher'),

    url(r'^search/$', SearchView.as_view(), name = 'catalog-search'),

    url(r'^comparison/$', ComparisonView.as_view(), name = 'catalog-comparison'),

    url(r'^product-reminder/$', ProductReminderView.as_view(), name = 'catalog-product-reminder'),

    url(r'^compare/add/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', AddToCompare.as_view(), name='catalog-compare-add'),

    url(r'^compare/remove/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$', RemoveFromCompare.as_view(), name='catalog-compare-remove'),

    url(r'^list/(?P<slug>[\w-]+)/(?P<brand_slug>[\w-]+)/$', BrandView.as_view(), name = 'catalog-brand'),

    url(r'^(?P<slug>[\w-]+)/$', CategoryView.as_view(), name = 'catalog-category'),

    url(r'^list/(?P<slug>[\w-]+).html$', SubCategoryView.as_view(), name = 'catalog-subcategory'),

    url(r'^products/(?P<slug>[\w-]+).html$', ProductView.as_view(), name = 'catalog-product'),

    url(r'^change-paginate-by/(?P<paginate_by>[\w]+)/$', ChangePaginateBy.as_view(), name = 'catalog-change-paginate-by'),

    # yandex market
    url(r'^market\.xml$', TSportYmarket.as_view(), name='yandex_market'),
)