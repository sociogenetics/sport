# -*- coding: utf-8 -*-
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import get_current_site
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.utils.safestring import mark_safe
from django.views.generic import DetailView, ListView, TemplateView, CreateView, RedirectView
import re
from bs4 import BeautifulSoup

from xendor.cbv import ToStructureMixin, VisibleObjectListMixin, ListByObjectSlugMixin, PaginatedListMixin,\
    SubtreeByObjectSlugMixin, RefererRedirectWithMessage, SortingMixin, PageAppExtensionMixin, SearchByModelMixin, \
    ContentTypeMixin, WithMailer, JSONResponseMixin
from xendor.settings import XendorSettings

import time
import simplejson
import base64
import hashlib

from app.catalog.models import Category, Product, Photo, Parameter, ProductToParameters, Brand, ProductReminder


def remove_tags(html, tags):
    """Returns the given HTML with given tags removed."""

    tags = [re.escape(tag) for tag in tags.split()]
    tags_re = '(%s)' % '|'.join(tags)

    starttag_re = re.compile(r'<%s(/?>|(\s+[^>]*>))' % tags_re, re.U)

    endtag_re = re.compile('</%s>' % tags_re)
    html = starttag_re.sub('', html)
    html = endtag_re.sub('', html)
    return html


def clean_tags(text, removed_tags='font span'):
    """Чистка атрибутов тегов"""

    #удаление шаринга
    text = text.split('<script charset')[0]

    #удаление лишних кавычек
    text = text.replace(u'\\"', '')
    text = text.replace(u'\\&quot;', '')

    #удаление лишних тегов
    text = remove_tags(text, tags=removed_tags)
    if not text:
        return ''

    soup = BeautifulSoup(text)


    tags = ['p', 'ul', 'li', 'a', 'h1', 'h2', 'h3', 'h4', 'td', 'table', 'tr', 'strong', 'b', 'div', 'img']
    for tag in tags:
        ftag = soup.findAll(tag)
        for i in ftag:
            #чистка стилей
            for key in i.attrs.keys():
                if key not in ('href', 'title', 'alt', 'src', 'rel', 'width', 'height', 'class'):
                    del i[key]

        if tag == 'a':
            for i in ftag:
                if 'a' in i.attrs.keys():
                    if len(i['href'].split('http:/')) > 1:
                        link = i['href'].split('http:/')[1]
                        if link[0] == '/':
                            link = link[1:]
                        i['href'] = 'http://' + link

    if soup.body:
        return soup.body.renderContents()
    return text


class Index(PageAppExtensionMixin, VisibleObjectListMixin):
    """Главная страница каталога"""

    template_name = 'catalog/index.html'
    model = Product
    paginate_by = 3
    app_extension = 'catalog'

    def get_queryset(self):

        return super(Index, self).get_queryset().filter(is_new=True).order_by('?')


class CategoryView(VisibleObjectListMixin, SubtreeByObjectSlugMixin):
    """Категория каталога"""

    template_name = 'catalog/category.html'
    model = Category


class SubCategoryView(PaginatedListMixin, VisibleObjectListMixin, ListByObjectSlugMixin, SortingMixin):
    """Категория каталога"""

    template_name = 'catalog/subcategory.html'
    model = Product
    slugified_model = Category
    sort_field = (('price', 'по цене', True), ('name', 'по названию', True), )
    default_sorting = ('price', False)

    def get_paginate_by(self, queryset):
        if self.request.session.get('paginate_by', '24') == 'all':
            return 10000
        return self.request.session.get('paginate_by', '24')

    def get_queryset(self):
        queryset = super(SubCategoryView, self).get_queryset()
        filtrs = [i for i in self.request.GET.items() if i[0].split('_')[0] == 'filtr' and i[1] != '0']
        for filtr in filtrs:
            queryset = queryset.filter(
                pk__in=ProductToParameters.objects.filter(
                    parameter_id=int(filtr[0].split('_')[1]),
                    value=filtr[1]
                ).order_by('product').values_list('product')
            )

        return queryset

    def get_context_data(self, **kwargs):
        context = super(SubCategoryView, self).get_context_data(**kwargs)
        products = self.get_queryset()
        parameters = Parameter.objects.filter(category__in=products.order_by('category').values_list('category'))
        res_pars = []
        for parameter in parameters:
            parameter.product_values = [(i[0], self.request.GET.get('filtr_'+str(parameter.id)) == i[0]) for i in ProductToParameters.objects.\
                filter(product__in=products, parameter=parameter).exclude(value='').distinct().order_by('value').values_list('value')]

            if parameter.product_values:
                res_pars.append(parameter)

        context.update({
            'parameters': res_pars,
            'is_filtered': bool([i for i in self.request.GET.items() if i[0].split('_')[0] == 'filtr' and i[1] != '0']),
            'brands': Brand.objects.filter(pk__in=products.order_by('brand').values_list('brand'))
        })

        return context


class BrandView(ToStructureMixin, SubCategoryView):

    template_name = 'catalog/brand.html'
    activated_node = lambda o: o.get_object().get_absolute_url()
    meta_title = lambda o: o.brand.meta_title or o.brand.name
    meta_description = lambda o: o.brand.meta_description or o.brand.name
    meta_keywords = lambda o: o.brand.meta_keywords or o.brand.name
    breadcrumbs = lambda o: o.brand.name

    def get_queryset(self):
        queryset = super(BrandView, self).get_queryset()

        try:
            self.brand = get_object_or_404(Brand, slug=self.kwargs['brand_slug'])
            queryset = queryset.filter(brand=self.brand)
        except KeyError:
            pass

        return queryset

    def get_context_data(self, **kwargs):
        context = super(BrandView, self).get_context_data(**kwargs)
        context.update({
            'brand': self.brand
        })
        return context


def signMessage(b64, secret):
    """sdfsdfs"""

    message = unicode(b64 + secret)
    m = hashlib.md5()
    m.update(message)
    sh = hashlib.sha1()
    sh.update(message)
    result = m.hexdigest() + sh.hexdigest()

    for i in xrange(1102):
        m = hashlib.md5()
        m.update(result)
        result = m.hexdigest()

    return result


class ProductView(ToStructureMixin, DetailView):
    """Страница товара"""

    model = Product
    template_name = 'catalog/product.html'

    activated_node = lambda o: reverse('catalog-index')
    meta_title = lambda o: o.get_object().meta_title or o.get_object().name
    meta_description = lambda o: o.get_object().meta_description or o.get_object().name
    meta_keywords = lambda o: o.get_object().meta_keywords or o.get_object().name

    breadcrumbs = lambda o: o.get_object().name

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)

        ts = time.time()
        partnerId = u'a06b000000L6NuwAAF'
        secretPhrase = u't-sport66-secret-L6Nuw431'
        orderNumber = ts

        order = {
            'items': [
                {
                    u'title': self.get_object().name,
                    u'category': self.get_object().category.all()[0].name,
                    u'qty': 1,
                    u'price': self.get_object().price
                }
            ],
            'partnerId': partnerId,
            'partnerOrderId': orderNumber,
            'partnerName': u'www.t-sport66.ru',
            'deliveryType': ''
        }

        json = simplejson.dumps(order)
        b64 = base64.b64encode(json)

        sig = signMessage(b64, secretPhrase)

        context.update({
            'parameters': ProductToParameters.objects.filter(product=self.get_object()).order_by('parameter__order'),
            'sig': sig,
            'b64': b64
        })

        return context


class SendKreditMessage(WithMailer, JSONResponseMixin, TemplateView):
    """Отсылка оповещения об оформлении кредита"""

    def get_context_data(self, **kwargs):
        self.send_mail(
            'Заявка на кредит на сайте {site_name}',
            'catalog/credit-mail.html',
            {'message': self.request.GET.get('message')},
            XendorSettings().get(u'E-mail администратора'))

        return {'ok': True}


class ComparisonView(PageAppExtensionMixin, TemplateView):
    """Страница сравнения товаров"""

    template_name = 'catalog/comparison.html'
    app_extension = 'comparison'

    def get_context_data(self, **kwargs):
        context = super(ComparisonView, self).get_context_data(**kwargs)
        compare = self.request.session.get('compare', {})
        items = [ContentType.
                    objects.
                    get_for_id(compare[x]['content_type_id']).
                    get_object_for_this_type(pk=compare[x]['object_id'])
                    for x in compare.keys()]

        parameters = Parameter.objects.filter(pk__in=ProductToParameters.objects.filter(product__in=items).order_by('parameter').values_list('parameter'))
        print parameters
        for parameter in parameters:
            parameter.values_list = []
            for item in items:
                value = ProductToParameters.objects.filter(parameter=parameter, product=item)
                if value:
                    value = value[0].value
                else:
                    value = '-'
                parameter.values_list.append(value)
        context.update({
            'items': items,
            'parameters': parameters
        })
        return context


class ChangePaginateBy(RefererRedirectWithMessage):
    """Изменение количества итемов на страницу"""

    action_message = u'Количество товаров отображаемых на странице изменено'

    def action(self):
        self.request.session['paginate_by'] = self.kwargs['paginate_by']


class SearchView(PageAppExtensionMixin, SearchByModelMixin, PaginatedListMixin, VisibleObjectListMixin, SortingMixin):
    """Поиск товаров"""

    template_name = 'catalog/search.html'
    model = Product
    sort_field = (('price', 'по цене', True), ('name', 'по названию', True),)
    search_fields = ['name', 'description']
    search_get_parameter = 's'
    app_extension = 'search'
    default_sorting = ('price', False)

    def get_paginate_by(self, queryset):
        if self.request.session.get('paginate_by', '24') == 'all':
            return 10000
        return self.request.session.get('paginate_by', '24')


class AddToCompare(ContentTypeMixin, RefererRedirectWithMessage):
    """добавление товара к списку сравнения"""

    action_message = u'Товар добавлен к сравнению'

    def action(self):
        compare = self.request.session.get('compare', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']

        compare[pk] = {
            'content_type_id': self.kwargs['content_type_id'],
            'object_id': self.kwargs['object_id']
        }
        self.request.session['compare'] = compare


class RemoveFromCompare(ContentTypeMixin, RefererRedirectWithMessage):
    """удаление товара из списка сравнения"""

    action_message = u'Товар удален из списка сравнения'

    def action(self):
        compare = self.request.session.get('compare', {})
        pk = self.kwargs['content_type_id'] + '-' + self.kwargs['object_id']
        del compare[pk]

        self.request.session['compare'] = compare


class ProductReminderView(ToStructureMixin, WithMailer, CreateView):
    """подписка на появление товара в продаже"""

    model = ProductReminder
    meta_title = meta_description = breadcrumbs = u'Подиска на появление товара'
    success_url = '/'

    def get_template_names(self):
        if self.request.is_ajax():
            return ['callback/ajax.html']

        return super(ProductReminderView, self).get_template_names()

    def get_success_url(self):
        if self.request.is_ajax():
            return reverse('callback-success')

        return super(ProductReminderView, self).get_success_url()

    def form_valid(self, form):

        self.send_mail(
            'Подписка на появление товара на сайте {site_name}',
            'catalog/reminder-mail.html',
            form.cleaned_data,
            XendorSettings().get(u'E-mail администратора'))

        form.instance.save()

        from django.contrib import messages
        messages.add_message(self.request, messages.SUCCESS, 'Спасибо! Заявка отправлена администрации сайта.')
        return super(ProductReminderView, self).form_valid(form)


class CategoryRedirector(RedirectView):
    """Редиректор для категорий"""

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        category = Category.objects.filter(old_id=kwargs['old_pk'])
        if category:
            return category[0].get_absolute_url()

        raise Http404


class ProductRedirector(RedirectView):
    """Редиректор для товаров"""

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        product = Product.objects.filter(old_id=kwargs['old_pk'])
        if product:
            return product[0].get_absolute_url()

        raise Http404


class YMarket(TemplateView):
    """Выгрузка товаров в яндекс маркет в соотвествии с форматом YML

    параметры:
    category_model - модель категорий
    item_model - модель товаров
    local_delivery_cost - стоимость локальной доставки

    модель товаров должна иметь следующие методы:
    - get_absolute_url->url - ссылка на товар
    - get_ymarket_available->true|false - доступность товара (доступен|предзаказ)
    - get_price->float - получить цену
    - get_category->ID - айдишник категории из секции categories файла YML
    - get_picture->url - линк на картинку товара
    - get_vendor->str  - производитель тупо буквами (если есть)
    - get_model->str   - название модели (например BMW X6)
    - get_description->CDATA - описание товара

    ###пример реализации в модели###
    @models.permalink
    def get_absolute_url(self):
        return ('catalog-product', [self.slug])

    def get_price(self):
        return float(self.price)

    def get_picture(self):
        try:
            return self.image.url
        except: pass

    def get_category(self):
        try:
            return self.category.all()[0]
        except: pass

    def get_ymarket_available(self):
        return (self.delivery_status == 'in_stock') and 'true' or 'false'

    def get_vendor(self):
        if self.brand:
            return self.brand.name

    def get_model(self):
        return self.name

    def get_description(self):
        return self.short_description

    ###типовой шаблон###
    <?xml version="1.0" encoding="utf-8"?>
    <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
    {% load cache xendor_tags %}
    <yml_catalog date="{% now "Y-m-d H:i" %}">
        <shop>
            <name>T-Sport</name>
            <company>ООО Т-Спорт</company>
            <url>http://t-sport66.ru</url>
            <currencies>
                <currency id="RUR" rate="1" plus="0"/>
            </currencies>
            <categories>
                {% for category in categories %}
                    <category id="{{ category.id }}"{% if category.parent_id %} parentId="{{ category.parent_id }}"{% endif %}>{{ category.name }}</category>
                {% endfor %}
            </categories>
            <local_delivery_cost>{{ local_delivery_cost }}</local_delivery_cost>
            <offers>
                {% for offer in offers %}
                    {% with offer.get_category as offer_category %}
                        {% if offer_category %}
                            <offer id="{{ offer.pk }}" type="vendor.model" available="{{ offer.get_ymarket_available|default:'true' }}">
                                <url>http://{{ site.domain }}{{ offer.get_absolute_url }}</url>
                                <price>{{ offer.get_price }}</price>
                                <currencyId>RUR</currencyId>
                                <categoryId>{{ offer_category }}</categoryId>
                                <picture>http://{{ site.domain }}{{ offer.get_picture }}</picture>
                                <name>{{ offer.name }}</name>
                                {% with offer.get_vendor as vendor %}
                                    {% if vendor %}
                                        <vendor>{{ vendor }}</vendor>
                                    {% endif %}
                                {% endwith %}
                                <model>{{ offer.get_model }}</model>
                                <description><![CDATA[{{ offer.get_description }}]]></description>
                            </offer>
                        {% endif %}
                    {% endwith %}
                {% endfor %}
            </offers>
        </shop>
    </yml_catalog>
    """
    content_type = 'application/xhtml+xml'
    template_name = 'catalog/ymarket.xml'
    category_model = Category
    item_model = Product
    local_delivery_cost = 0

    def get_categories(self):
        """Список категорий"""
        return self.category_model.objects.all()

    def get_offers(self):
        """Товары"""
        return self.item_model.objects.filter(visible=True)

    def get_local_delivery_cost(self):
        """Локальная стоимость доставки"""
        return self.local_delivery_cost

    def get_context_data(self, **kwargs):
        context = super(YMarket, self).get_context_data(**kwargs)

        context.update({
            'site': get_current_site(self.request),
            'local_delivery_cost': self.get_local_delivery_cost(),
            'categories': self.get_categories(),
            'offers': self.get_offers()
        })

        return context


class TSportYmarket(YMarket):
    """Выгрузка в YMarket"""

    def get_offers(self):
        return super(TSportYmarket, self).get_offers().filter(to_yml=True)


class CalcFetcher(TemplateView):
    """ получение содержимого калькулятора"""

    template_name = 'catalog/calc-fetcher.html'

    def get_context_data(self, **kwargs):
        import urllib2
        context = super(CalcFetcher, self).get_context_data(**kwargs)
        response = urllib2.urlopen('http://im.tk-kit.ru/calc/?c=c95f5c8d47659d07cb5fe44d1176642c')
        html = response.read()
        context.update({'content': html})
        return context


###
#<iframe src="/im.tk-kit.ru.proxy.php?c=c95f5c8d47659d07cb5fe44d1176642c" style="width:320px;height:570px;outline:none;border:none;border-radius:0px;"></iframe>
#<?php echo file_get_contents("http://im.tk-kit.ru/calc/?".$_SERVER['QUERY_STRING']);?>
#Парсинг старых данных
###
# from app.catalog.models import PhpshopCategories, PhpshopProducts, PhpshopFoto
# from django.core.files import File
#
#
# def parse_category(category=None, level=0):
#     """Обработка категорий"""
#
#     if category:
#         subcategories = PhpshopCategories.objects.using('old').filter(parent_to=category.id).order_by('num')
#     else:
#         subcategories = PhpshopCategories.objects.using('old').filter(parent_to=0).order_by('num')
#
#     for subcategory in subcategories:
#         print '--- ' * level + subcategory.name
#
#         new = Category(
#             name=subcategory.name,
#             description=subcategory.content,
#             meta_title=subcategory.title,
#             meta_description=subcategory.descrip,
#             meta_keywords=subcategory.keywords,
#             old_id=subcategory.id
#         )
#
#         if category:
#             new.insert_at(Category.objects.get(old_id=category.id), position='last-child', save=True)
#         else:
#             Category.objects.insert_node(new, None, position='last-child', save=True)
#
#         parse_category(subcategory, level=level+1)
#
#
# def parse_item():
#     """Обработка товаров"""
#
#     log = open('parse_log.txt', 'w')
#     parser_error = []
#     base_pic_path = '/Users/tahy/workspace/projects/sport/tsport/sport/public'
#
#     products = PhpshopProducts.objects.using('old')
#
#     for product in products:
#         #print product.name
#
#         new = Product(
#             name=product.name,
#             short_description=product.description,
#             description=product.content,
#             price=product.price,
#             old_price=product.price_n,
#             meta_title=product.title,
#             meta_description=product.descrip,
#             meta_keywords=product.keywords,
#             old_id=product.id
#         )
#         new.save()
#         try:
#             new.category.add(Category.objects.get(old_id=product.category))
#         except Category.DoesNotExist:
#             parser_error.append(u'Не найдена категория с id %s' % product.category)
#
#         #Загрузка основной картинки
#         try:
#             f = open(base_pic_path + product.pic_big, 'r')
#         except IOError:
#             parser_error.append(u'Отсутствует картинка для товара %s с id %s' % (product.name, product.id,))
#
#         pic_file = File(f)
#         new.image = pic_file
#         new.save()
#
#         f = pic_file = None
#
#         #Загрузка дополнительных картинок
#         photos = PhpshopFoto.objects.using('old').filter(parent=product.id)
#         for photo in photos:
#             try:
#                 f = open(base_pic_path + photo.name, 'r')
#             except IOError:
#                 parser_error.append(u'Отсутствует доп. картинка %s для товара %s с id %s' % (photo.id, product.name, product.id,))
#
#             pic_file = File(f)
#             pic = Photo(
#                 product=new,
#                 image=pic_file
#             )
#             pic.save()
#
#     #print parser_error
#     for row in parser_error:
#         log.write((row + u'\n').encode("utf-8"))
#
#     log.close()
#
#
# class ParserView(TemplateView):
#     """Парсер данных со старого сайта"""
#
#     template_name = 'catalog/parser.html'
#
#     def post(self, request, *args, **kwargs):
#         ##код парсинга
#
#         parser_error = []
#
#         #парсинг категорий
#         #parse_category()
#
#         #парсинг товаров
#         #parse_item()
#
#         return super(ParserView, self).get(request, *args, **kwargs)
#
#     def get_context_data(self, **kwargs):
#         context = super(ParserView, self).get_context_data(**kwargs)
#
#         try:
#             log = open('parse_log.txt', 'r')
#             context['not_parsed'] = log.readlines()
#         except IOError:
#             context['not_parsed'] = []
#         return context