# -*- coding: utf-8 -*-
from django import template
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from ..models import Product, PopularCategory, Category, ReminderForm

from xendor.settings import XendorSettings

register = template.Library()

@register.inclusion_tag('catalog/tags/popular.html')
def catalog_popular_category():
    """популярные категории"""

    return {'popular': PopularCategory.objects.filter(visible=True)}


@register.inclusion_tag('catalog/tags/special-for-main.html')
def catalog_special_for_main():
    """спецпредложения на главной"""

    cat_list = []
    for category in Category.objects.filter(special=True):
        cat_list += category.get_descendants(include_self=True)

    return {
        'result': Product.objects.filter(
            category__in=cat_list, visible=True).exclude(old_price=0).order_by('?')[:4]
    }


@register.inclusion_tag('catalog/tags/special.html')
def catalog_special_by_category(category):
    """спецпредложения для категории"""

    return {
        'products': Product.objects.filter(
            category__in=category.get_descendants(include_self=True), visible=True
        ).exclude(old_price=0).order_by('?')[:4]
    }


@register.inclusion_tag('catalog/tags/product-group.html')
def catalog_product_group(product):
    """Вместе_с_товаром_также_покупают блок"""

    return {
        'products': product.product_group.filter(visible=True)
    }



@register.inclusion_tag('catalog/tags/paginate-by.html', takes_context=True)
def catalog_paginate_by(context):
    """Селектор пагинатора"""

    return {
        'current': context['request'].session.get('paginate_by', '24')
    }


@register.simple_tag
def catalog_compare_url(url_pattern, content_object):
    """рендер урла для ссылки с использованием контент-объекта"""

    content_type = ContentType.objects.get_for_model(content_object.__class__)
    return reverse(url_pattern, args=[content_type.pk, content_object.pk])


@register.assignment_tag(takes_context=True)
def catalog_item_in_comparison(context, content_object):
    """Проверка наличия объекта в списке сравнения"""

    compare = context['request'].session.get('compare', {})
    content_type = ContentType.objects.get_for_model(content_object.__class__)
    pk = str(content_type.pk) + '-' + str(content_object.pk)
    return compare.get(pk, False)


@register.inclusion_tag('catalog/tags/product-reminder.html')
def catalog_reminder_form(product):
    """Окно подписаться на появление товара"""

    return {
        'reminder_form': ReminderForm(initial={'product': product})
    }

