# -*- coding: utf-8 -*-
from django.contrib import admin, messages
from django import forms
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.views.generic import CreateView, FormView
from django.conf import settings
from django.db import models

from xendor.admin_utils import image_field
from xendor.cbv import RefererRedirectWithMessage
from xendor.tree_admin import XDP17TreeModelAdmin

from app.catalog.models import Product, Category, Photo, PopularCategory, Brand, Parameter, ProductToParameters, ProductReminder

import xlrd
import csv
from app.catalog.views import clean_tags


class PriceForm(forms.Form):
    ovelon = forms.FileField(widget=forms.ClearableFileInput(attrs={'class': "grp-button"}), label=u'Прайс Овелон xls')
    welcome = forms.FileField(widget=forms.ClearableFileInput(attrs={'class': "grp-button"}), label=u'Прайс Welcome xls')
    neotren = forms.FileField(widget=forms.ClearableFileInput(attrs={'class': "grp-button"}), label=u'Прайс Неотрен csv')


def clear_value(value):
    if type(value) is float:
        return unicode(int(value))

    return unicode(value)


def parse_price_file_ovalon(f, not_found_log, found_log):
    """Парсинг прайса овелон"""

    with open(settings.MEDIA_ROOT + '/ovalon.xls', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    not_found_row = []
    found_row = []

    xls = xlrd.open_workbook(settings.MEDIA_ROOT + '/ovalon.xls', encoding_override="cp1251")

    sheet = xls.sheets()[0]
    result = []
    for i in range(sheet.nrows):
        if i < 9:
            continue

        cells = sheet.row_slice(i)

        if cells[3].value == '':
            continue

        #код добавления товара

        pname = cells[0].value.strip()
        product = Product.objects.filter(Q(name=pname) | Q(xls_import_name=pname))
        if product:
            found_row.append([i+1, u'Товар &laquo;' + pname + u'&raquo;'])
            #обработка статуса товара
            result += [p.pk for p in product]
        else:
            not_found_row.append([i+1, u'Товар с названием &laquo;' + pname + u'&raquo; не найден в базе товаров'])

    not_found_log.write((u'\n\n\n\n' + u'##########Прайс Овелон###########' + u'\n\n').encode("utf-8"))
    for row in not_found_row:
        not_found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    found_log.write((u'\n\n\n\n' + u'##########Прайс Овелон############' + u'\n\n').encode("utf-8"))
    for row in found_row:
        found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    return result


def parse_price_file_welcome(f, not_found_log, found_log):
    """Парсинг прайса велком"""

    with open(settings.MEDIA_ROOT + '/welcome.xls', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    not_found_row = []
    found_row = []

    xls = xlrd.open_workbook(settings.MEDIA_ROOT + '/welcome.xls', encoding_override="cp1251")
    sheet = xls.sheets()[0]

    result = []
    for i in range(sheet.nrows):
        if i < 12:
            continue

        cells = sheet.row_slice(i)

        if cells[3].value == '':
            continue

        #код добавления товара
        pname = cells[2].value.strip()
        product = Product.objects.filter(Q(name=pname) | Q(xls_import_name=pname))
        if product:
            found_row.append([i+1, u'Товар &laquo;' + pname + u'&raquo;'])
            #обработка статуса товара
            result += [p.pk for p in product]
        else:
            not_found_row.append([i+1, u'Товар с названием &laquo;' + pname + u'&raquo; не найден в базе товаров'])

    not_found_log.write((u'\n\n\n\n' + u'##########Прайс Велком#########' + u'\n\n').encode("utf-8"))
    for row in not_found_row:
        not_found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    found_log.write((u'\n\n\n\n' + u'##########Прайс Велком##########' + u'\n\n').encode("utf-8"))
    for row in found_row:
        not_found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    return result


def parse_price_file_neotren(f, not_found_log, found_log):
    """Парсинг прайса неотрен"""

    with open(settings.MEDIA_ROOT + '/neotren.csv', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    not_found_row = []
    found_row = []

    def cp1251_to_utf_8_encoder(unicode_csv_data):
        for line in unicode_csv_data:
            yield line.decode('cp1251').encode("UTF-8")

    csvfile = open(settings.MEDIA_ROOT + '/neotren.csv', 'rb')
    reader = csv.reader(cp1251_to_utf_8_encoder(csvfile), delimiter=';')
    result = []
    i = 0
    for row in reader:
        if i == 0:
            i += 1
            continue

        pname = unicode(row[4], "UTF-8")

        #код добавления товара
        product = Product.objects.filter(Q(name=pname) | Q(xls_import_name=pname))
        if product:
            found_row.append([i+1, u'Товар &laquo;' + pname + u'&raquo;'])
            #обработка статуса товара
            result += [p.pk for p in product]
        else:
            not_found_row.append([i+1, u'Товар с названием &laquo;' + pname + u'&raquo; не найден в базе товаров'])

        i += 1

    not_found_log.write((u'\n\n\n\n' + u'##########Прайс Неотрен###########' + u'\n\n').encode("utf-8"))
    for row in not_found_row:
        not_found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    found_log.write((u'\n\n\n\n' + u'##########Прайс Неотрен###########' + u'\n\n').encode("utf-8"))
    for row in found_row:
        found_log.write((u'строка:' + unicode(row[0]) + ', ' + row[1] + u'\n').encode("utf-8"))

    return result


class CategoryParameterInline(admin.TabularInline):
    model = Parameter
    extra = 0


class CategoryAdmin(XDP17TreeModelAdmin):
    search_fields = ('name',)
    list_filter = ('visible', 'special',)
    list_per_page = 10000
    mptt_indent_field = "parent"
    inlines = [CategoryParameterInline]

admin.site.register(Category, CategoryAdmin)


class ImagesInline(admin.TabularInline):
    model = Photo
    extra = 0


class ProductParametersForm(forms.ModelForm):

    def __init__(self, **kwargs):
        super(ProductParametersForm, self).__init__(**kwargs)

        try:
            obj = kwargs['instance']
            choices = [('', '---Пусто---')] + [(i.strip(), i.strip()) for i in obj.parameter.values.split("\n") if i.strip()]
        except:
            choices = None

        if choices:
            self.fields['value'].widget.choices = choices

    class Meta:
        model = ProductToParameters
        widgets = {
            'value': forms.Select()
        }


class ProductParametersInline(admin.TabularInline):
    model = ProductToParameters
    extra = 0
    fields = ('parameter_name', 'value',)
    readonly_fields = ('parameter_name',)
    form = ProductParametersForm

    def parameter_name(self, instance):
        return '<a href="%s" target="_blank">%s</a>' % ('/admin/catalog/product/%s/' % instance.pk, instance)

    parameter_name.short_description = "Параметр"
    parameter_name.allow_tags = True

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj):
        return False


def set_status_in_stock(modeladmin, request, queryset):
    for item in queryset:
        item.delivery_status = 'in_stock'
        item.save()
    #queryset.update(delivery_status='in_stock')

set_status_in_stock.short_description = "Сменить наличие на \"В наличии\""


def set_status_orderable(modeladmin, request, queryset):
    for item in queryset:
        item.delivery_status = 'orderable'
        item.save()
    #queryset.update(delivery_status='orderable')

set_status_orderable.short_description = "Сменить наличие на \"Под заказ\""


def set_status_out_stock(modeladmin, request, queryset):
    for item in queryset:
        item.delivery_status = 'out_stock'
        item.save()
    #queryset.update(delivery_status='out_stock')

set_status_out_stock.short_description = "Сменить наличие на \"Отсутствует\""


def multiset_parameters(modeladmin, request, queryset):
    if request.POST.get('post'):
        for k in request.POST:
            if len(k.split('pars_')) > 1:
                product_id = k.split('_')[1]
                parameter_id = k.split('_')[2]
                in_db = ProductToParameters.objects.get(parameter=parameter_id, product=product_id)
                in_db.value = request.POST.get(k)
                in_db.save()
        modeladmin.message_user(request, u'Характеристики товаров обновлены', level=messages.SUCCESS)
        return None

    for obj in queryset:

        parameters = Parameter.objects.filter(category__in=obj.category.all())
        for parameter in parameters:
            try:
                in_db = ProductToParameters.objects.get(parameter=parameter, product=obj)
            except ProductToParameters.DoesNotExist:
                to_db = ProductToParameters(
                    parameter=parameter,
                    product=obj,
                    value=''
                )
                to_db.save()

        paremeters_to_set = ProductToParameters.objects.filter(product=obj)
        for parameter in paremeters_to_set:
            parameter.choices = [('', '---Пусто---')] + [(i.strip(), i.strip()) for i in parameter.parameter.values.split("\n") if i.strip()]

        obj.parameters_to_set = paremeters_to_set

    return render_to_response('catalog/admin/multiset-parameters.html', {'products': queryset}, context_instance=RequestContext(request))

multiset_parameters.short_description = "Установить характеристики товаров"


def set_categories(modeladmin, request, queryset):
    if request.POST.get('post'):
        to_db_categories = request.POST.getlist('category_set')
        to_db_categories = Category.objects.filter(pk__in=to_db_categories)

        for product in queryset:
            product.category.clear()
            product.category.add(*to_db_categories)

        modeladmin.message_user(request, u'Категории товаров установлены', level=messages.SUCCESS)
        return None

    categories = Category.objects.all()
    for category in categories:
        category.leveled_name = '- - ' * category.level

    return render_to_response('catalog/admin/set-categories.html', {'categories': categories, 'products': queryset}, context_instance=RequestContext(request))

set_categories.short_description = "Установить категорию"


class ProductAdmin(admin.ModelAdmin):
    list_per_page = 50
    list_display = 'pk', 'poster', 'name', 'xls_import_name', 'price', 'old_price', 'delivery_status', 'is_new', 'visible', 'check_by_price', 'to_yml'
    list_display_links = 'poster',
    list_editable = 'xls_import_name', 'is_new', 'visible', 'check_by_price', 'delivery_status', 'to_yml'
    filter_horizontal = 'category', #'product_group',
    list_filter = 'visible', 'category', 'brand', 'delivery_status', 'is_new'
    search_fields = 'id', 'name',
    poster = image_field('image', u'Изображение', u'50;50;fix')
    inlines = [ImagesInline, ProductParametersInline]
    actions = [set_status_in_stock, set_status_orderable, set_status_out_stock, multiset_parameters, set_categories]
    raw_id_fields = 'product_group',
    save_as = True
    change_list_template = 'catalog/admin/list.html'
    change_form_template = 'catalog/admin/editable-form.html'

    def get_urls(self):
        urls = super(ProductAdmin, self).get_urls()

        prices = patterns('',  url(r'^load-price/$', login_required(ProductAdmin.LoadPrice.as_view()), {}),)

        parameters = patterns('', url(r'^(.+)/init-parameters/$', login_required(ProductAdmin.InitParameters.as_view()), {}),)

        clear_text = patterns('', url(r'^clear-text/$', ProductAdmin.ClearText.as_view(), {}),)

        return clear_text + parameters + prices + urls

    class ClearText(RefererRedirectWithMessage):

        action_message = u'Тексты обработаны'

        def action(self):

            for item in Category.objects.all()[:1000]:
                item.description = clean_tags(item.description, 'font span')
                item.save()

            for product in Product.objects.all():
                product.short_description = clean_tags(product.short_description, 'font span h2 h1 h3')
                product.description = clean_tags(product.description)
                product.save()

            pass

    class InitParameters(RefererRedirectWithMessage):

        action_message = u'Параметры товара инициализированы'

        def action(self):
            obj = get_object_or_404(Product, pk=self.args[0])
            parameters = Parameter.objects.filter(category__in=obj.category.all())
            for parameter in parameters:
                try:
                    in_db = ProductToParameters.objects.get(parameter=parameter, product=obj)
                except ProductToParameters.DoesNotExist:
                    to_db = ProductToParameters(
                        parameter=parameter,
                        product=obj,
                        value=''
                    )
                    to_db.save()

    class LoadPrice(FormView):

        template_name = 'catalog/admin/load-price.html'
        form_class = PriceForm

        def get_success_url(self):
            return reverse('admin:' + 'catalog_product_changelist') + 'load-price/'

        def get_context_data(self, **kwargs):
            context = super(ProductAdmin.LoadPrice, self).get_context_data(**kwargs)
            context['back_link'] = reverse('admin:' + 'catalog_product_changelist')

            #инициализация отслеживаемых позиций
            #Product.objects.exclude(xls_import_name='').update(check_by_price=True)

            try:
                log = open(settings.MEDIA_ROOT + '/price_not_found.txt', 'r')
                context['not_parsed'] = log.readlines()
            except IOError:
                context['not_parsed'] = []
            try:
                log = open(settings.MEDIA_ROOT + '/price_found.txt', 'r')
                context['parsed'] = log.readlines()
            except IOError:
                context['parsed'] = []

            return context

        def form_valid(self, form):

            not_found_log = open(settings.MEDIA_ROOT + '/price_not_found.txt', 'w')
            found_log = open(settings.MEDIA_ROOT + '/price_found.txt', 'w')

            #ставим статус доставки в "отсутствует" у всех отслеживаемых товаров
            Product.objects.filter(check_by_price=True).update(delivery_status=u'out_stock')

            in_stock = []
            try:
                in_stock += parse_price_file_ovalon(self.request.FILES['ovelon'], not_found_log, found_log)
            except:
                found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))
                not_found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))

            try:
                in_stock += parse_price_file_welcome(self.request.FILES['welcome'], not_found_log, found_log)
            except:
                found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))
                not_found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))

            try:
                in_stock += parse_price_file_neotren(self.request.FILES['neotren'], not_found_log, found_log)
            except:
                found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))
                not_found_log.write((u'\n\n\n\n' + u'<<<<<<<<<<<<<Ошибка обработки прайсу>>>>>>>>>>>>>' + u'\n\n').encode("utf-8"))

            #обновляем статусы товаров на "в наличии"
            Product.objects.filter(pk__in=in_stock).update(delivery_status=u'in_stock')

            not_found_log.close()
            found_log.close()

            messages.add_message(self.request, messages.SUCCESS, u'Прайс обработан')
            return super(ProductAdmin.LoadPrice, self).form_valid(form)




admin.site.register(Product, ProductAdmin)


class PopularCategoryAdmin(admin.ModelAdmin):

    list_display = 'poster', 'name', 'visible'
    list_display_links = 'poster', 'name'
    filter_horizontal = 'categories',
    list_per_page = 10000
    poster = image_field('image', u'Изображение', u'50;50;fix')


admin.site.register(PopularCategory, PopularCategoryAdmin)


class BrandAdmin(admin.ModelAdmin):

    list_display = 'poster', 'name',
    list_display_links = 'poster', 'name'

    poster = image_field('image', u'Изображение', u'50;50;fix')

admin.site.register(Brand, BrandAdmin)


class ProductReminderAdmin(admin.ModelAdmin):

    list_display = 'product', 'mail', 'created', 'is_send'
    list_display_links = 'product', 'mail'
    list_filter = 'is_send',
    readonly_fields = 'product', 'send_data',

admin.site.register(ProductReminder, ProductReminderAdmin)
